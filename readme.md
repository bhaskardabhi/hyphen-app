# Setup

Clone Repo
Create .env file in root (Example given below or copy .env.example and modify)
Run Migration using `php artisan migrate` (It will setup database)
Run server by `php -S localhost:8000 -t public`

# Setup Ref
[https://lumen.laravel.com/docs/5.8](https://lumen.laravel.com/docs/5.8)

# APIs

APIs are given in swagger.yaml file in root of the directory

## `.Env` Example
APP_NAME=Lumen
APP_ENV=local
APP_KEY=
APP_DEBUG=true
APP_URL=http://localhost
APP_TIMEZONE=UTC

LOG_CHANNEL=stack
LOG_SLACK_WEBHOOK_URL=

DB_CONNECTION=mysql
DB_HOST=localhost
DB_PORT=3306
DB_DATABASE=hyphen_app
DB_USERNAME=root
DB_PASSWORD=password

CACHE_DRIVER=file
QUEUE_CONNECTION=sync