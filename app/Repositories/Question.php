<?php

namespace App\Repositories;

use App\Models\Question as QuestionModel;
use App\Models\Answer as AnswerModel;

class Question{
    public $with = [];

    public function with(array $with = []){
        $this->with = $with;
        return $this;
    }

    public function paginate(){
        return (new QuestionModel)->with($this->with)->paginate();
    }

    public function save(array $data, $id = null){
        return app('db')->transaction(function () use ($data, $id){
            $question = $id ? $this->find($id) : (new QuestionModel);
            $question->title = $data['title'];
            $question->save();

            $answers = isset($data['answers']) ? $data['answers'] : [];

            foreach($answers as $answer){
                $answerModel = isset($answer['id']) && $answer['id'] ? (new AnswerModel)->find($answer['id']) : (new AnswerModel);
                $answerModel->title = $answer['title'];
                $answerModel->question_id = $question->id;
                $answerModel->deleted_at = isset($answer['deleted_at']) ? $answer['deleted_at'] : null;
                $answerModel->save();
            }

            return $question;
        });
    }

    public function find(int $id){
        return (new QuestionModel)->with($this->with)->findOrFail($id);
    }

    public function delete(int $id){
        return !!(new QuestionModel)->find($id)->delete();
    }
}
