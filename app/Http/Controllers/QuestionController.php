<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\Question;

class QuestionController extends Controller{
    public function all(Request $request){
        return app()->make(Question::class)->with(
            $request->with ? explode(',',$request->with) : []
        )->paginate();
    }

    public function store(Request $request){
        $this->validate($request, [
            'title' => 'required',
            'answers' => 'required|between:2,5',
            'answers.*.title' => 'required'
        ],[
            'answers.required' => 'Answers are required to create question',
            'answers.between' => 'Question must have a minimum of two answers and a maximum of 5 answers',
        ]);

        return app()->make(Question::class)->save($request->all());
    }

    public function show(Request $request, $id){
        return app()->make(Question::class)->with(
            $request->with ? explode(',',$request->with) : []
        )->find($id);
    }

    public function update(Request $request, $id){
        return [
            'success' => !!app()->make(Question::class)->save($request->all(), $id)
        ];
    }

    public function delete($id){
        return [
            'success' => app()->make(Question::class)->delete($id)
        ];
    }
}
